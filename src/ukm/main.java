package ukm;

public class main {

    public static void main(String[] args) {
        System.out.println("============================================");
        System.out.println("============ Menghitung Iuran ==============");
        System.out.println("============================================");
        ukm u = new ukm("UKM Futsal Fst");

        mahasiswa maha = new mahasiswa();
        maha.setnama("Paguh");
        maha.setnim("195314146");
        maha.settempatTgllahir("Singkawang, 15 mei 2001");
        u.setketua(maha);
         
        mahasiswa sekre = new mahasiswa("195314401", "Cindy", "Jawai, 17 april 1999");
        u.setsekretaris(sekre);
        penduduk[] pend = new penduduk[3];
        mahasiswa[] m = new mahasiswa[2];
        m[0] = new mahasiswa();
        m[0].setnama("Viki");
        m[0].setnim("195314301");
        m[0].settempatTgllahir("Balikpapan, 17 juni 2000");
        pend[0] = m[0];

        m[1] = new mahasiswa();
        m[1].setnama("Leo");
        m[1].setnim("195314143");
        m[1].settempatTgllahir("Surabaya, 18 Desember 1999");
        pend[1] = m[1];

        masyarakatSekitar[] mas = new masyarakatSekitar[1];
        mas[0] = new masyarakatSekitar();
        mas[0].setnama("Tri");
        mas[0].setnomor("981");
        mas[0].settempatTgllahir("Jakarta, 25 Desember 1998");
        pend[2] = mas[0];

        System.out.println("");
        System.out.println("==============="+u.getnamaUnit()+"===============");
        System.out.println("");
        double tot = 0;
        double iuranMaha = 0, iuranMasya = 0;
        for (int i = 0; i < pend.length; i++) {
            System.out.println("ANGGOTA KE " + (i + 1) + "");
            System.out.println("nama : " + pend[i].getnama());
            System.out.println("Tempat Tanggal Lahir : " + pend[i].gettempatTgllahir());
            if (pend[i] instanceof mahasiswa) {
                mahasiswa mahas = (mahasiswa) pend[i];
                System.out.println("Nim : " + mahas.getnim());
                System.out.println("Iuran : " + mahas.hitungIuran());
                iuranMaha = +mahas.hitungIuran();
            } else if (pend[i] instanceof masyarakatSekitar) {
                masyarakatSekitar masy = (masyarakatSekitar) pend[i];
                System.out.println("Nomor : " + masy.getnomor());
                System.out.println("Iuran : " + masy.hitungIuran());
                iuranMasya = +masy.hitungIuran();
            }
            System.out.println("");
            System.out.println("============================================");
        }
tot = iuranMaha + iuranMasya;
        System.out.println("Total Iuran : "+tot);
    }

}
